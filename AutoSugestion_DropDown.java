import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutoSugestion_DropDown {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Alan.Hidalgo\\Downloads\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(); 
		driver.get("https://ksrtc.in/oprs-web/");
		driver.manage().window().maximize();
		
		driver.findElement(By.id("fromPlaceName")).sendKeys("BEN");
		Thread.sleep(2000L);
		driver.findElement(By.id("fromPlaceName")).sendKeys(Keys.DOWN);
		
		System.out.println(driver.findElement(By.id("fromPlaceName")).getText());
		
		//Ejecutor javascript
		JavascriptExecutor js = (JavascriptExecutor)driver;
		
		String script = "return document.getElementById(\"fromPlaceName\").value;";
		String text = (String) js.executeScript(script);
		System.out.println(text);
		
		int i = 0;
		
		while(!text.equalsIgnoreCase("BENGALURU INTERNATION AIRPORT")) 
		{
			i++;
			driver.findElement(By.id("fromPlaceName")).sendKeys(Keys.DOWN);
			text = (String) js.executeScript(script);
			System.out.println(text);
			
			if(i>10) 
			{
				break;
			}
		}
		
		if(i>10) 
		{
			System.out.println("No se encontro elemento");
		}
	}

}

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Objetos_Desplegables_DropDown {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Alan.Hidalgo\\Downloads\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.spicejet.com/");
		driver.manage().window().maximize();
		
		driver.findElement(By.id("ctl00_mainContent_ddl_originStation1_CTXT")).click();
		
		driver.findElement(By.xpath("//a[@value= 'BLR']")).click();
		Thread.sleep(1000L);
		driver.findElement(By.xpath("(//a[@value='MAA'])[2]")).click();
		
		//fecha
		driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/div[1]/table/tbody/tr[4]/td[4]/a")).click();
		
		//Pasajeros
		driver.findElement(By.id("divpaxinfo")).click();
		//Adultos
		Select adultos = new Select(driver.findElement(By.id("ctl00_mainContent_ddl_Adult")));
		Thread.sleep(1000L);
		adultos.selectByValue("3");
		
		//ni�os
		Select ni�os = new Select(driver.findElement(By.id("ctl00_mainContent_ddl_Child")));
		Thread.sleep(1000L);
		ni�os.selectByValue("4");
	}

}
